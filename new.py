#! /usr/bin/env python
# -*- coding: utf-8 -*-
# order by => --order date,relevance,viewCount | Default = relevance
#note that author_link tag is the thumbnail URL and uniqueId is the channel name

from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.tools import argparser
import feedgenerator
import requests
import json
# Set DEVELOPER_KEY to the API key value from the APIs & auth > Registered apps
# tab of
#   https://cloud.google.com/console
# Please ensure that you have enabled the YouTube Data API for your project.



DEVELOPER_KEY = 'your_developer_key_here'



YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"
def youtube_search(options):
  youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
    developerKey=DEVELOPER_KEY)
  # Call the search.list method to retrieve results matching the specified
  # query term.
  search_response = youtube.search().list(
    q=options.q,
    part="id,snippet",
    maxResults=options.max_results,
    order=options.order
  ).execute()
  feed = feedgenerator.Rss201rev2Feed(
     title=options.q,
     link="https://www.youtube.com/results?search_query=%s"%options.q,
     description=options.order,
     language="en",
 )
  # Add each result to the appropriate list, and then display the lists of
  # matching videos, channels, and playlists.
  for search_result in search_response.get("items", []):
    if search_result["id"]["kind"] == "youtube#video":
      titletag = search_result["snippet"]["title"]
      linktag = "http://www.youtube.com/watch?v="+(search_result["id"]["videoId"])
      channelNametag = search_result["snippet"]["channelTitle"]
      response = requests.get('https://www.googleapis.com/youtube/v3/videos?part=snippet&id=%s&key=%s' %(search_result["id"]["videoId"],DEVELOPER_KEY))
      newjson = json.loads(response.content, "utf-8")
      newjsonthumbnail = newjson["items"][0]["snippet"]["thumbnails"]["high"]["url"]
      newjson = newjson["items"][0]["snippet"]["description"]
      if newjson == "":
        newjson = "Video does not have description"
      feed.add_item(
        title=titletag,
        link=linktag,
        description=newjson,
        unique_id=channelNametag,
        author_link=newjsonthumbnail
      )
      with open("feeds/"+options.q+".rss", 'w') as fp:
        feed.write(fp, 'utf-8')
if __name__ == "__main__":
  argparser.add_argument("--q", help="Search term", default="Google")
  argparser.add_argument("--max-results", help="Max results", default=20)
  argparser.add_argument("--order", help="order", default="relevance")
  args = argparser.parse_args()
  try:
    youtube_search(args)
  except HttpError, e:
    print "An HTTP error %d occurred:\n%s" % (e.resp.status, e.content)