# Installation #

### To install this plugin to your WordPress website follow the guide below: ###


```
sudo apt install python-pip
which python => output should be /usr/bin/python
sudo pip install requests
sudo pip install feedgenerator
sudo pip install feedparser
sudo pip install --upgrade google-api-python-client
git clone 
cd youtuberssfeeds
sudo mv new.py /var/www/html/wp-admin/
sudo mv updatefeeds.py /var/www/html/wp-admin/
```
In the next line the number refers to every x minutes that the rss feeds are updated. Change this number if needed.) 
```
echo "*/1 * * * * /usr/bin/python /var/www/html/wp-admin/updatefeeds.py" >> newcron.txt
crontab newcron.txt
```


[Get your own API key](https://console.cloud.google.com) =>
Create your own project
In the menu, click on API Manager
click on enable api
search and select YouTube Data API v3
click enable.
In the api manager menu => go to credentials
copy the key for API key 1
```
sudo nano /var/www/html/wp-admin/new.py
```
paste your developer key inside 
```
DEVELOPER_KEY = 'your_developer_key_here'
```